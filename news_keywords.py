# 필요한 패키지 임포트
import re
from bs4 import BeautifulSoup
import urllib.request
from stop_words import get_stop_words
import os
import sys



def get_words():
    urls= ['https://news.google.com/topics/CAAqJggKIiBDQkFTRWdvSUwyMHZNRGx6TVdZU0FtVnVHZ0pWVXlnQVAB/sections/CAQiN0NCQVNJd29JTDIwdk1EbHpNV1lTQW1WdUdnSlZVeUlQQ0FRYUN3b0pMMjB2TURsNU5IQnRLQUEqKggAKiYICiIgQ0JBU0Vnb0lMMjB2TURsek1XWVNBbVZ1R2dKVlV5Z0FQAVAB?hl=en-US&gl=US&ceid=US%3Aen',
            'https://news.google.com/topics/CAAqJggKIiBDQkFTRWdvSUwyMHZNRGx6TVdZU0FtVnVHZ0pWVXlnQVAB/sections/CAQiN0NCQVNJd29JTDIwdk1EbHpNV1lTQW1WdUdnSlZVeUlQQ0FRYUN3b0pMMjB2TUdkbWNITXpLQUEqKggAKiYICiIgQ0JBU0Vnb0lMMjB2TURsek1XWVNBbVZ1R2dKVlV5Z0FQAVAB?hl=en-US&gl=US&ceid=US%3Aen']
    news_titles = []
    text = []
    for url in urls :
        html = urllib.request.urlopen(url)
        soup = BeautifulSoup(html,'html.parser')
        titles= soup.find_all('a','ipQwMb')
        for title in titles :
            news_titles.append(title.find('span').get_text())

        titles= soup.find_all('a','ipQwMb Q7tWef')
        for title in titles :
            news_titles.append(title.find('span').get_text())

        titles= soup.find_all('a','ipQwMb Q7tWef')
        for title in titles :
            news_titles.append(title.find('span').get_text())
    for title in news_titles :
        letters_only = re.sub('[^a-zA-Z]', ' ',title)
        words = letters_only.lower().split()
        stops = set(get_stop_words('en'))
        meaningful_words = [w for w in words if not w in stops]
        text.append(' '.join(meaningful_words))

    return text

def get_top20(texts) :
    d_count = {}   #동시출현 빈도가 저장될 dict
    for line in texts:
        words = list(set(line.split()))   #단어별로 분리한 것을 set에 넣어 중복 제거하고, 다시 list로 변경
        for i, a in enumerate(words):
            for b in words[i+1:]:
                if a == b: continue   #같은 단어의 경우는 세지 않음
                if a > b: a, b = b, a   #A, B와 B, A가 다르게 세어지는것을 막기 위해 항상 a < b로 순서 고정
                d_count[a, b] = d_count.get((a, b), 0) + 1   #실제로 센다
    items = [[v,k] for k,v in d_count.items()]
    items.sort()
    items.reverse()
    items = [[k, v] for v, k in items]

    dictset = []
    for item in items[:20]:
        x,y = item[0]
        dictset.append(x+' & '+y)
    return dictset


def translate_keywords(words) :
    client_id = "jJZwDPSQnX11Xup_Lri1"    # 자신이 제공 받은 KEY ID
    client_secret = "1px1W5FxLx"   # 자신이 제공 받은 KEY 암호
    encText = urllib.parse.quote(words)
    data = "source=en&target=ko&text=" + encText
    url = "https://openapi.naver.com/v1/papago/n2mt"
    request = urllib.request.Request(url)
    request.add_header("X-Naver-Client-Id",client_id)
    request.add_header("X-Naver-Client-Secret",client_secret)
    response = urllib.request.urlopen(request, data=data.encode("utf-8"))
    rescode = response.getcode()
    if(rescode==200):
        response_body = response.read()
        return response_body.decode('utf-8')
    else:
        print("Error Code:" + rescode)

def get_list(dictset) :
    return [translate_keywords(sets)[152:-4] for sets in dictset]
