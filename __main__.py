
# 메인 함수
import news_keywords as nk
import sys

def main():
    result_text = nk.get_words()
    dictset =  nk.get_top20(result_text)
    keywords = nk.get_list(dictset)
    print(keywords, file = sys.stderr)
    return {"Keywords": nk.get_list(dictset)}


if __name__ == '__main__':
    main()
